# APLA

APLA (adaptive piecewise linear approximation) is a Python library for solving non-convex and non-linear power dispatches. The idea is to build a sequence of piecewise approximation of the objective and solve it successively until we reach a given tolerance.

## Installation

Simply clone the repository on your computer.

```
git clone https://gitlab.com/Loicvh/apla
```

## Dependencies

This repository depends on

- Multiple python libraries: numpy, csv, os, tikzplotlib, ...

- Python Gurobi API, install it [here](https://www.gurobi.com/documentation/8.1/refman/py_python_api_overview.html). Free academic license can be obtained on their website.

- [MatPower](https://matpower.org/) for the data.

- Octave to run MatPower.

## Usage

Up to now the usage is strictly limited to UNIX system. This is mainly due to the way other OS manage files.

To run the full tests and obtain the same figures as [1], simply enters:

```
sh run.sh
```
The returned figures are stored in ```/output/``` and the number of trials (N) and different cases can be changed inside ```run.sh```.

In order to change the IEEE case, change the CASE in ```run.sh```, any IEEE case from MatPower (see [here](https://matpower.org/docs/ref/matpower5.0/menu5.0.html)) should work. The parameters of the added VPE units are located in ```data/vpe/gencost.csv```, the duplicates are created with the python3 function ```data/vpe/duplicate_vpe.py```. In order to obtain the second set of Figures from [1], replace the content of ```gencost.csv``` by the content of ```gencost_20.csv``` and run ```run.sh```.

## Problem Definition
The problem of interest here is the economic dispatch which consists in dispatching the electrical demand among the commited power unit in order to meeth the system load at minimal cost. The objective function is subjected to operational constraints such as the min and max power range, ramping constraints and network related constraints.


## Method Description

Two methods are implemented: the first one, APLA, always search on the whole feasible set. On the other hand, the second one is a heuristic, denoted as H or master, which starts to search on the whole feasible set, and then restricts the search around points of interest. Two variant of the heuristic are considered here, depending if the restricted search space is small (H-local or master-local) or large (H-full or master-full).

Theoretically, and in the special example considered here, this method suffers from the fact that the approximations are not *stricto sensu* under-approximations.


## TODO

- Remove slave and use solveProblem everywhere, or vice-versa.

- Move all fun-based functions to fun.py such that the method could be easily extended to different objective functions - and not limit it to the sum of a quadratic part and a rectified sine.

## Contributing
Pull requests, comments and questions are welcome. 

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Sources
[1]. L. Van Hoorebeeck, P.-A. Absil and A. Papavasiliou, Global Solution of Economic Dispatch with Valve Point Effect and Transmission Constraints, 2020. Preprint.
