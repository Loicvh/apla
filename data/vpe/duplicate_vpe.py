#!/usr/bin/env python3
# 
# This simple script duplicates the VPE generators in a 
# stochastic way to reduce unwanted symmetry
#
# For simplicity, it should be launch from this directory

import numpy as np
np.random.seed(0)
str_init = "gencost_init.csv"
n_dup = 2


P = np.genfromtxt(str_init, delimiter=',')
n, m = P.shape
print(n, m)
n_gen = n_dup*n
str_out = "gencost_"+str(n_gen)+".csv"

P_out = np.zeros((n*n_dup, m))
print(P.shape)
P_out[0:n, :] = P

for j in range(1, n_dup):
    R = (np.random.random_sample((n, m))-0.5)/5
    _P = P + R * P
    P_out[n*j:n*(j+1), :] = _P
np.savetxt(str_out, P_out, delimiter=',')
