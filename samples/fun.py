import numpy as np
import math


eps_p = pow(10, -6)


def f_tot(x, P):
    cumSum = 0
    if len(x.shape) == 1:
        for i in range(0, x.size):
            par = P[i]
            cumSum = cumSum + f(x[i], par)
    elif len(x.shape) == 2:
        for t in range(0, x.shape[0]):
            for i in range(0, x.shape[1]):
                par = P[i]
                cumSum = cumSum + f(x[t, i], par)
    else:
        raise ValueError  # TOCHANGE
    return cumSum


def f(x, par):
    a, b, c, d, e, p_min, p_max = getPar(par)
    return a*pow(x, 2)+b*x+c+d*abs(np.sin(e*(x-p_min)))


def getPar(par):
    return par[0], par[1], par[2], par[3], par[4], par[5], par[6]


def getKinkMid(par, s=2):
    a, b, c, d, e, p_min, p_max = getPar(par)
    p = []
    k = 0
    if e == 0 or d == 0:
        p = [p_min, p_max]
    else:
        while k == 0 or p[k-1] < p_max:
            new_p = p_min + k*math.pi/(s*e)
            p.append(new_p)
            k = k+1
            if abs(math.sin(e*(new_p-p_min))) > eps_p:
                pass
        p[-1] = p_max

    return p
