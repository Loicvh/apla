#! python3

# Global import
from gurobipy import *
import numpy as np
import bisect
import time
import os


# Local import
import getData
import fun

alg_dflt = 'APLA'
flag_warm_start = False


eps_p = pow(10, -6)


def Problem(alg=alg_dflt):
    # PROBLEM(alg=alg_dflt) creates an optimization model of the economic dispatch
    #
    # return:   mod: the model variable of gurobipy
    #           gen_VPE: set of generator obeying a VPE
    #           x_kink: set of kink point
    #           xi: set of knots
    #           P: parameters, see getP
    #           p: gurobi variable
    #           p_VPE: gurobi variable
    #
    # This function uses Gurobi Python API with data obtained through getData.
    # A valid gurobi license is needed. Three algorithms are supported:
    # APLA (Adaptive Piecewise Linear Approximation)
    # APQUA (Adaptive Piecewise-Quadratic Under-Approximation)
    # QP_no_VPE (Quadratic Programming while ignoring the VPE)

    B, bus, bus_generator, bus_load, D, from_bus, gen, gen_fun_param, lines, loads, n, p_max,\
            p_min, RD, RU, T, TC, to_bus = getData.getData()
    P = getP(gen, gen_fun_param, p_min, p_max)
    D_tot = {}
    # Sum all load to get the load at each bus
    for b in bus:
        for t in T:
            dic_foo = {t: D[(t, l)] for l in loads if bus_load[l] == b}
            D_tot[(t, b)] = sum(dic_foo.values())
    TC_max = TC
    for l in lines:
        if TC_max[l] == 0:
            TC_max[l] = float('inf')
    TC_min = {l: -TC_max[l] for l in lines}
    bus_T = [(t, b) for t in T for b in bus]
    gen_T = [(t, g) for t in T for g in gen]
    gen_VPE = getGenVPE(P, gen)
    gen_VPE_T = [(t, g) for t in T for g in gen_VPE]
    x_kink = {g: fun.getKinkMid(P[g], 2) for g in gen_VPE}

    lines_T = [(t, l) for t in T for l in lines]
    p_max_T = {(t, g): p_max[g] for (t, g) in gen_T}
    p_min_T = {(t, g): p_min[g] for (t, g) in gen_T}
    TC_max_T = {(t, l): TC_max[l] for (t, l) in lines_T}
    TC_min_T = {(t, l): TC_min[l] for (t, l) in lines_T}
    xi = {(t, g): fun.getKinkMid(P[g], 2) for t in T for g in gen_VPE}
    m = {(t, g): len(x_kink[g]) for t in T for g in gen_VPE}
    mod = Model('EconomicDispatch')
    mod.Params.mipgap = 0.001
    p = mod.addVars(gen_T, lb=p_min_T, ub=p_max_T, name='p')
    # p_VPE is useless and trivially equals to 0 for alg=(APLA or QP_no_VPE)
    p_VPE = mod.addVars(gen_VPE_T, lb=p_min_T, ub=p_max_T, name='p_VPE') 
    f = mod.addVars(lines_T, lb=TC_min_T, ub=TC_max_T, name='f')
    theta = mod.addVars(bus_T, name='theta')
    for (t, b) in bus_T:
        mod.addConstr(
            D_tot[t, b] == sum(f[t, l] for l in lines if from_bus[l] == b)
            - sum(f[t, l] for l in lines if to_bus[l] == b)
            + sum(p[t, g] for g in gen if bus_generator[g] == b),
            name='FlowConservation[%s,%s]' % (t, b))
    for (t, k) in lines_T:
        mod.addConstr(
            f[t, k] - B[k]*(theta[t, from_bus[k]] - theta[t, to_bus[k]]) == 0,
            name='Susceptance[%s,%s]' % (t, k))

    if len(T) > 1:
        for t_idx in range(1, len(T)):
            t_now = T[t_idx]
            t_before = T[t_idx-1]
            for g in gen:
                mod.addConstr(-RD[g] <= p[t_now, g] - p[t_before, g], name='RampDown')
                mod.addConstr(p[t_now, g] - p[t_before, g] <= RU[g], name='RampUp')

    cumSum = 0
    for g in gen_VPE:
        if m[T[0], g] == 1:
            cumSum += fun.f(x_kink[g][0], P[g])
    if alg == 'APLA':
        mod.setObjective(len(T)*cumSum+quicksum((gen_fun_param[g][0]*p[t, g]*p[t, g]
                                                 + gen_fun_param[g][1]*p[t, g]
                                                 + gen_fun_param[g][2])
                                                for (t, g) in gen_T if g not in gen_VPE),
                         GRB.MINIMIZE)
        updatePWL(gen_VPE, p, T, m, alg, mod, xi, P)
    elif alg == 'APQUA':
        # Gurobi API does not allow a 'usual' objective along with a piecewise one.
        # Hence we use two identical variable p and p_vpe.
        mod.addConstrs(p[t, g] == p_VPE[t, g] for (t, g) in gen_VPE_T)
        mod.setObjective(len(T)*cumSum+quicksum((gen_fun_param[g][0]*p[t, g]*p[t, g]
                                                 + gen_fun_param[g][1]*p[t, g]
                                                 + gen_fun_param[g][2])
                                                for (t, g) in gen_T),
                         GRB.MINIMIZE)
        updatePWL(gen_VPE, p_VPE, T, m, alg, mod, xi, P)
    elif alg == 'QP_no_VPE':
        mod.setObjective(quicksum((gen_fun_param[g][0]*p[t, g]*p[t, g]
                                   + gen_fun_param[g][1]*p[t, g]
                                   + gen_fun_param[g][2])
                                  for (t, g) in gen_T),
                         GRB.MINIMIZE)
    else:
        raise ValueError('Error algorithm %s not known', alg)

    return mod, gen_VPE, x_kink, xi, P, p, p_VPE


def solveProblem(alg=alg_dflt, n_iter=10, flag_write=False):
    # SOLVEPROBLEM(alg=alg_dflt, n_iter=10, flag_write) solves the problem using the algorithm
    #                                                   alg with n_iter max iteration and
    #                                                   save results in /tmp if flag_write.
    #
    # return best_obj, gen_VPE, m, mod, p, p_VPE, P, x_kink, xi
    # return:   best_obj: the best true objective
    #           gen_VPE: set of generator obeying a VPE
    #           m: length of knots
    #           mod: the model variable of gurobipy
    #           p: gurobi variable
    #           p_VPE: gurobi variable
    #           P: parameters, see getP
    #           x_kink: set of kink point
    #           xi: set of knots
    #
    # This function uses Gurobi Python API to solve the problem defined with getProblem.
    # A valid gurobi license is needed. Three algorithms are supported:
    # APLA (Adaptive Piecewise Linear Approximation)
    # APQUA (Adaptive Piecewise-Quadratic Under-Approximation)
    # QP_no_VPE (Quadratic Programming while ignoring the VPE)

    tic_solveProblem = time.time()
    root_dir, _ = os.path.split(os.path.dirname(os.path.abspath(__file__)))
    B, bus, bus_generator, bus_load, D, from_bus, gen, gen_fun_param, lines, loads, n, p_max,\
        p_min, RD, RU, T, TC, to_bus = getData.getData()
    mod, gen_VPE, x_kink, xi, P, p, p_VPE = Problem(alg)
    m = {(t, g): len(x_kink[g]) for t in T for g in gen_VPE}
    P_list = [P[g] for g in gen]
    mod.write(os.path.join(root_dir, 'tmp/gurobi/EconomicDispatch.lp'))
    best_bound = 0
    best_obj = float('inf')
    k = 0
    flagInsert = True
    delta_tol = 10
    delta = delta_tol+1
    delta_local = delta
    mod.Params.timeLimit = 45
    if alg == 'QP_no_VPE':
        print('QP no VPE algorithm')
        mod.optimize()
        k = n_iter
        sol = mod.getAttr('x', p)
        sol_list = [[sol[t, g] for g in gen] for t in T]
        true_obj = getTrueObj(T, sol_list, P_list)
        print('Real objective value', true_obj)
        print('Surrogate value', mod.objVal)
        obj_VPE = getObjVPE(T, gen_VPE, sol, P)
        print('VPE cost', obj_VPE)
        delta = true_obj - mod.objVal
        print('Delta', delta)
        percentage = delta/obj_VPE*100
        print('Percentage', percentage)
        best_obj = true_obj
        k = n_iter  # to be sure to not go in the loop
    else:
        if flag_warm_start:
            # Warm start
            obj_QP, _, _, mod_foo, p_foo, _, _, _ = solveProblem(alg='QP_no_VPE')
            mod_foo.update()
            print('Preloading with Quadratic with objective', obj_QP)
            print(mod_foo.ObjVal)
            #for t in T:
                #for g in gen:
                    #p[t, g].Start = p_foo[t, g].X #TODO TO CHECK?
    while k < n_iter and flagInsert and delta_local > delta_tol:
        mod.update()
        print('\n \n ************************* \n Master iteration number %d \n ************************* \n\n' % k)
        print("Reseting model to avoir MIP start search")
        mod.reset()
        # mod.Params.ConcurrentMIP = 2
        mod.optimize()
        if mod.isMIP:
            best_bound = max(best_bound, mod.ObjBound)
        else:
            best_bound = max(best_bound, mod.ObjVal)

        # Updating knots
        flagInsert = False
        nInsort = 0
        for t in T:
            for g in gen_VPE:
                idx = insortTol(xi[t, g], p[t, g].x, eps_p)  # Insortion made here if needed!
                if idx > -1:
                    nInsort += 1
                    flagInsert = True
        sol = mod.getAttr('x', p)
        sol_list = [[sol[t, g] for g in gen] for t in T]
        true_obj = getTrueObj(T, sol_list, P_list)
        best_obj = min(true_obj, best_obj)
        obj_VPE = getObjVPE(T, gen_VPE, sol, P)
        print('VPE cost', obj_VPE)
        print("Total  number of insortion is", nInsort)
        print("Flag insertion is", flagInsert)
        print("Surrogate objective", mod.objVal)
        print("True objective", true_obj)
        print("Best true objective is", best_obj)
        print("Best bound is", best_bound)
        delta_local = true_obj - mod.objVal
        delta = best_obj - best_bound
        print("Delta is", delta)
        print("Delta local is", delta_local)
        if alg == "APQUA":
            updatePWL(gen_VPE, p_VPE, T, m, alg, mod, xi, P)
        else:
            updatePWL(gen_VPE, p, T, m, alg, mod, xi, P)

        k = k+1
    opt_gap = (best_obj - best_bound)/best_obj*100
    print('Optimality gap is %s %%' % opt_gap)
    if flag_write:
        # real_bound and exec_time seem useless but are used in
        # myfile.write(str(eval(var))) !
        real_bound = best_bound
        exec_time = time.time() - tic_solveProblem
        dirs = ['real_bound', 'best_obj', 'opt_gap', 'exec_time']
        if alg == 'QP_no_VPE':
            dirs.append('percentage')
        for var in dirs:
            with open(os.path.join(root_dir, "tmp/", alg, "case"+str(len(bus))+"_"+str(var)+".txt"), "a") as myfile:
                myfile.write(str(eval(var))+"\n")
    return best_obj, best_bound, gen_VPE, m, mod, p, p_VPE, P, x_kink, xi


def master(string_file='master', restrict_method='local', alg=alg_dflt):
    # MASTER solves the problem over the whole feasible set. Then, it uses SLAVE to enhance
    # the previously obtained incumbents via a local approach.

    tic_master = time.time()
    n_iter = 2
    B, bus, bus_generator, bus_load, D, from_bus, gen, gen_fun_param, lines, loads, n, p_max, \
        p_min, RD, RU, T, TC, to_bus = getData.getData()
    best_obj, best_bound, gen_VPE, m, mod, p, p_VPE, P, x_kink, xi = solveProblem(alg, n_iter)
    real_bound = best_bound
    # mod.Params.SolCount = 3 #TODO TO CHANGE
    nSolutions = mod.SolCount
    root_dir, _ = os.path.split(os.path.dirname(os.path.abspath(__file__)))
    tg = {(t, g) for t in T for g in gen}
    list_pi = []
    p_list = []
    obj_hist = []
    bound_hist = []
    sur_hist = []
    nSolutions = min(10, nSolutions)  # TODO 10 enough\too much
    print('Number of solutions found: ' + str(nSolutions))
    p_e = [[p[t, g].Xn for g in gen] for t in T]
    best_sol = np.array(p_e)
    for e in range(0, nSolutions):
        mod.setParam(GRB.Param.SolutionNumber, e)
        p_e = [[p[t, g].Xn for g in gen] for t in T]
        p_list.append(p_e)
        list_pi.append({(t, g): p[t, g].Xn for t in T for g in gen})
        incumbent_obj = f_tot(T, gen, p, P)
        print('Incumbent real objective is', incumbent_obj)
        if incumbent_obj < best_obj:
            best_obj = incumbent_obj
            best_sol = np.array(p_e)
            print('Objective set as incumbent !')
    for e in range(0, nSolutions):
        Xi = {(t, g): x_kink[g] for g in gen_VPE for t in T}
        restrictRange(p, list_pi[e], Xi, m, mod, gen_VPE, T, eps_p, restrict_method)
        print('Best obj before slave', best_obj)
        #mod.Params.Cutoff = best_obj
        mod.Params.BestBdStop = best_obj
        #for (t, g) in tg:
            #p[t, g].Start = list_pi[e][t, g] # TODO TO CHECK
        print('Entering Slave number', e)
        best_obj, sur_obj, best_sol = slave(mod, p, p_VPE, m, eps_p, P, Xi, tg, best_obj, best_sol, gen_VPE, alg)
        print('Best obj after slave', best_obj)
        obj_hist.append(best_obj)
        bound_hist.append(best_bound)
        sur_hist.append(sur_obj)
    print('Real bound is', real_bound)
    print('Best objective is', best_obj)
    opt_gap = (best_obj - real_bound)/best_obj*100
    print('Optimality gap is %s %%', opt_gap)
    exec_time = time.time() - tic_master # /! used below
    for var in ['real_bound', 'best_obj', 'opt_gap', 'exec_time']:
        with open(os.path.join(root_dir,"tmp/", string_file+'-'+restrict_method, "case"+str(len(bus))+"_"+str(var)+".txt"), "a") as myfile:
            myfile.write(str(eval(var))+"\n")


def slave(mod, p, p_VPE, m, eps_p, P, xi, tg, best_obj, best_sol, gen_VPE, alg):
    # Could be replaced by solveProblem

    root_dir, _ = os.path.split(os.path.dirname(os.path.abspath(__file__)))
    mod.Params.mipgap = 0.0001  # 10 times lower than master
    mod.Params.timelimit = 10
    n_iter = 10
    B, bus, bus_generator, bus_load, D, from_bus, gen, gen_fun_param, lines, loads, n, p_max,\
        p_min, RD, RU, T, TC, to_bus = getData.getData()
    flagInsert = True
    delta_tol = 10
    delta_loc = delta_tol + 1
    best_bound_loc = 0
    sur_obj = []
    k = 0
    while k < n_iter and flagInsert and delta_loc > delta_tol:
        print('\n ************************* \n Slave iteration number %d \n ************************* \n' % k)
        #for (t, g) in tg:
            #p[t, g].start = p[t, g].x #TODO TO CHECK

        # Solving
        mod.update()
        mod.optimize()
        mod.write(os.path.join(root_dir, 'tmp', 'gurobi', 'slave.lp'))
        print('Status is', mod.status)
        if mod.status in (3, 6, 15): # 3 Unfeasible 6 ? 15 Best bound reached
            return best_obj, 'inf', best_sol
        if mod.isMIP:
            best_bound_loc = max(best_bound_loc, mod.ObjBound)
        else:
            best_bound_loc = max(best_bound_loc, mod.ObjVal)
        
        # Updating knots
        flagInsert = False
        nInsort = 0
        for t in T:
            for g in gen_VPE:
                idx = insortTol(xi[t, g], p[t, g].x, eps_p)
                if idx > -1:
                    print('Insertion in ', t, g)
                    nInsort += 1
                    flagInsert = True
        # Updating Piecewise fun
        if alg == 'APQUA':
            updatePWL(gen_VPE, p_VPE, T, m, alg, mod, xi, P)
        elif alg == 'APLA':
            updatePWL(gen_VPE, p, T, m, alg, mod, xi, P)
        sur_obj.append(mod.ObjVal)
        list_p = []
        # Print number of solutions stored
        nSolutions = min(mod.SolCount, 10)
        print('Number of solutions found: ' + str(nSolutions))
        for e in range(nSolutions):
            mod.setParam(GRB.Param.SolutionNumber, e)
            if nSolutions > 1:
                list_p.append({(t, g): p[t, g].Xn for t in T for g in gen})
            else:
                list_p.append({(t, g): p[t, g].x for t in T for g in gen})
            if mod.SolCount > 1:
                incumbent_obj = f_tot(T, gen, p, P)
            else:
                incumbent_obj = f_tot_2(T, gen, p, P)

            print('Incumbent real objective is', incumbent_obj)
            if incumbent_obj < best_obj:
                print('Old value', best_obj)
                best_obj = incumbent_obj
                print('New value', best_obj)
                best_p = p
                sol = mod.getAttr('x', best_p)
                best_sol = [[sol[t, g] for t in T] for g in gen]
                print('\n\n**** Objective set as incumbent !****\n\n')
        print("Total  number of insortion is", nInsort)
        print("Flag insertion is", flagInsert)
        print("Surrogate objective", mod.objVal)
        if not mod.Params.status == 15:
            if mod.SolCount > 1:
                true_obj = f_tot(T, gen, p, P)
            else:
                true_obj = f_tot_2(T, gen, p, P)

            print("True objective", true_obj)
            print("Best real objective is", best_obj)
            print("Best (local) bound is", best_bound_loc)
            delta_loc = best_obj - mod.objVal
            delta = best_obj - best_bound_loc
            print("Delta local is", delta_loc)
            print("Delta is", delta)
        k += 1
    return best_obj, mod.objVal, best_sol


def restrictRange(p_foo, p, Xi, m, mod, gen_VPE, T, eps_p, restrict_method):
    if restrict_method == 'local':
        restrictRangeLocal(p_foo, p, Xi, m, mod, gen_VPE, T, eps_p)
    elif restrict_method == 'full':
        restrictRangeFull(p_foo, p, Xi, m, mod, gen_VPE, T, eps_p)
    elif restrict_method == 'new':
        restrictRangeNew(p_foo, p, Xi, m, mod, gen_VPE, T, eps_p)
    else:
        raise ValueError('Invalid restrict_method argument: restrict_method\
                         should be local or full, current value is', restrict_method)


def restrictRangeLocal(p_foo, p, Xi_tot, m, mod, gen_VPE, T, eps_p):
    for g in gen_VPE:
        for t in T:
            for j in range(1, m[t, g]):
                if p[t, g] < Xi_tot[t, g][j]:
                    p_foo[t, g].ub = Xi_tot[t, g][j]
                    p_foo[t, g].lb = Xi_tot[t, g][j-1]
                    break
                elif abs(p[t, g] - Xi_tot[t, g][j]) <= eps_p and j < m[t, g]-1:
                    p_foo[t, g].ub = Xi_tot[t, g][j+1]
                    p_foo[t, g].lb = Xi_tot[t, g][j-1]
                    break
                elif j == m[t, g]-1:
                    p_foo[t, g].lb = Xi_tot[t, g][j-1]
                    p_foo[t, g].ub = Xi_tot[t, g][j]


def restrictRangeFull(p_foo, p, Xi_tot, m, mod, gen_VPE, T, eps_p):
    for g in gen_VPE:
        for t in T:
            for j in range(2, m[t, g]-2):
                if j == m[t, g]-3:
                    p_foo[t, g].lb = Xi_tot[t, g][j-2]
                    p_foo[t, g].ub = Xi_tot[t, g][j+2]
                    break
                elif p[t, g] < Xi_tot[t, g][j]:
                    p_foo[t, g].ub = Xi_tot[t, g][j+2]
                    p_foo[t, g].lb = Xi_tot[t, g][j-2]
                    break
                elif abs(p[t, g] - Xi_tot[t, g][j]) <= eps_p and j < m[t, g]-2:
                    p_foo[t, g].ub = Xi_tot[t, g][j+2]
                    p_foo[t, g].lb = Xi_tot[t, g][j-2]
                    break

def restrictRangeNew(p_foo, p, Xi_tot, m, mod, gen_VPE, T, eps_p):
    for g in gen_VPE:
        for t in T:
            for j in range(1, m[t, g]):
                if p[t, g] < Xi_tot[t, g][j]:
                    mod.addConstr(Xi_tot[t, g][j-1] <= p_foo[t, g])
                    mod.addConstr(Xi_tot[t, g][j] >= p_foo[t, g])
                    Xi_tot[t, g] = [0, j, m[t, g]-1]
                    break
                elif abs(p[t, g] - Xi_tot[t, g][j]) <= eps_p and j < m[t, g]-1:
                    mod.addConstr(Xi_tot[t, g][j-1] <= p_foo[t, g])
                    mod.addConstr(Xi_tot[t, g][j+1] >= p_foo[t, g])
                    break
                elif j == m[t, g]-1:
                    mod.addConstr(Xi_tot[t, g][j-1] <= p_foo[t, g])
                    mod.addConstr(Xi_tot[t, g][j] >= p_foo[t, g])


def getTrueObj(T, sol_list, P):
    true_obj = 0
    for t in range(len(T)):
        true_obj += fun.f_tot(np.asarray(sol_list[t]), P)
    return true_obj


def getObjVPE(T, gen_VPE, sol, P):
    obj_VPE = 0
    for t in T:
        for g in gen_VPE:
            obj_VPE += fun.f(sol[t, g], P[g])
    return obj_VPE


def f_tot(T, gen, p, P, *mod):
    obj = 0
    for t in T:
        for g in gen:
            obj += fun.f(p[t, g].Xn, P[g])  # could raise exception if only one solution => .X
    return obj


def f_tot_2(T, gen, p, P, *mod):
    obj = 0
    for t in T:
        for g in gen:
            obj += fun.f(p[t, g].x, P[g])  # could raise exception if only one solution => .X
    return obj


def getGenVPE(P, gen):
    gen_VPE = []
    for g in gen:
        if P[g][4] != 0 or P[g][3] != 0:
            gen_VPE.append(g)
    return gen_VPE


def updatePWL(gen_VPE, p, T, m, alg, mod, xi, P):
    for g in gen_VPE:
        for t in T:
            if m[t, g] > 1:
                if alg == 'APLA':
                    mod.setPWLObj(p[t, g], xi[t, g], fun.f(np.asarray(xi[t, g]), P[g]))
                elif alg == 'APQUA':
                    mod.setPWLObj(p[t, g], xi[t, g], fun.f(np.asarray(xi[t, g]), [0, 0, 0]+P[g][3:]))


def insortTol(vec, x, eps_p):
    index = bisect.bisect_left(vec, x)
    if index == 0 or index == len(vec):
        return -1
    if x-vec[index-1] < eps_p or vec[index] - x < eps_p:
        return -1
    else:
        vec.insert(index, x)
    return index


def getP(gen, gen_fun_param, p_min, p_max):
    P = {G: gen_fun_param[G] + [p_min[G], p_max[G]] for G in gen}
    return P


def test_solveProblem():
    tic = time.time()
    solveProblem()
    toc = time.time() - tic
    print('Elapsed time is', toc)


def test_master(string_file='master', method='local'):
    tic = time.time()
    master(string_file, method)
    toc = time.time() - tic
    print('Elapsed time is', toc)


def main():
    master(restrict_method='full', alg='APLA')
    master(alg='APLA')
    solveProblem(alg="APLA", flag_write=True)
    solveProblem(alg="QP_no_VPE", flag_write=True)


if __name__ == "__main__":
    main()
