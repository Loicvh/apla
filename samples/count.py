import sys


def setCount(case_num):
    for var in ['real_bound', 'best_obj', 'opt_gap', 'exec_time']:
        my_sum = 0
        n = 0
        with open("../tmp/"+str(var)+"_"+str(case_num)+".txt", "r") as myfile:
            for s in myfile:
                my_sum += float(s)
                n += 1
            my_sum += float(s)
            n += 1
        with open("../output/number_"+str(case_num)+".txt", "a") as f:
            f.write(var + " " + str(my_sum/n)+"\n")


def main(argv):
    print("Launching script", argv[0])
    if len(argv) != 2:
        print("Invalide set of parameter")
    else:
        setCount(argv[1])


if __name__ == "__main__":
    main(sys.argv)
