#!/bin/sh

SCRIPT=$(readlink -f $0)
DIRECTORY=`dirname $SCRIPT`
echo ${DIRECTORY}
find $DIRECTORY/tmp/ -type f -name "*.txt" -delete

for CASE in "case118" "case57"
do
	echo "Case iteration ${CASE}"
	N=100


	for I in `seq 0 $N`;
	do
		echo "Bash iteration ${I}"
		echo $DATA_PATH
		echo "mpc = ${CASE};
		csvwrite('${DIRECTORY}/data/matpower/bus.csv', mpc.bus)
		csvwrite('${DIRECTORY}/data/matpower/gen.csv', mpc.gen)
		csvwrite('$DIRECTORY/data/matpower/branch.csv', mpc.branch)
		csvwrite('$DIRECTORY/data/matpower/gencost.csv', mpc.gencost)" > "$DIRECTORY/data/matpower/matpower_to_csv.m"

		octave -W "$DIRECTORY/data/matpower/matpower_to_csv.m"
		python3 "$DIRECTORY/data/splitData.py" 24 15 15 True True ${I} # T RD RU flag_VPE flag_multiple seed
		python3 "$DIRECTORY/samples/getProblem.py"
	done
done # CASE loop
python3 "$DIRECTORY/samples/plotBox.py"
